<link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
<link href="../css/sonoclick.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
<link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- BEGIN THEME GLOBAL STYLES -->
<link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
<link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
<!-- END THEME GLOBAL STYLES -->
<style>

    .table td.source_icon{
		vertical-align: middle;
	}

    .source_icon i{
		font-size: 30px;
	}

    .facebook_official i{
		color:#3b5998;
	}

    .table a.btn {
    	padding: 4px 7px;
	}

	img.listing_image {
		height: 35px;
		width: 36px; 
	}

	.fb_img img{
		border:1px solid #ccc;
		width:160px;
		height: 160px;
		margin:10px 0;
	}

	.helptext{
		background: #000 none repeat scroll 0 0;
		color: #fff;
		margin: 4px;
		padding: 0 5px;
	}

	.agree-check {
		padding-left: 35px;
	}

	span#agree-error {
		position: absolute;
		top: 15px;
		left: -20px;
	}

	textarea{ 
		max-width: 100%; 
	}

	input.other-box {
		display: none;
		border: 1px solid #ccc;
		opacity: 1;
		z-index: 1;
		margin-top: 10px;
		width: 200px;
		height: 25px;
		top: -10px;
		left: 75px;
	}

	input.other-box-radio {
		display: none;
		border: 1px solid #ccc;
		opacity: 1;
		z-index: 1;
		margin-top: 10px;
		width: 200px;
		height: 25px;
		top: -10px;
		left: 75px;
	}

	.google, .google>.portlet-title,.google .btn-primary {
		background-color: #ff0000 !important;
		border-color: #ff0000 !important;
	}

	.youtube_official{
		color: #ff0000 !important;
	}

	.view_form .flag_row .flag img{
		max-width:50px;
	}

	.view_form .flag_row .flag.active img{
		max-width:125px;
	}

	.advertising_row iframe{
		border:none;
	}

	.view_form_page .container{
		max-width: 1440px;
	}

	.view_form .social_row .text p{
		padding: 0;
		margin: 10px 0;
		font-size: 20px;
		text-transform: uppercase;
	}

	.view_form .fb_login_box p.heading{
		font-size: 20px;
		text-transform: uppercase;
		font-weight: 300;
	}

	.view_form .social_row .fb{
		text-align: right;
		border-right:none;
	}

	.view_form .social_row .fb a{
		display:block;
		text-align:right;
		width:100%;
	}

	.view_form .social_row .you_tube{
		text-align: left;
		min-height: 70px;
		display: flex;
		align-items: center;
	}

	.view_form .social_row .you_tube, .view_form .social_row .fb{
		min-height: 100px;
		display: flex;
		align-items: center;
		margin-bottom: 12px;
	}

	.view_form .social_row img {
		max-width: 180px;
	}

	.view_form .social_row .you_tube img {
		max-width: 165px;
	}

	.choose_one p{
		margin:20px 0 2px;
		font-weight:600
	}

	@media only screen and (min-device-width : 320px) and (max-device-width : 767px) {

		.view_form .social_row .text p{
			padding: 10px;
			margin: 0;
		}

		.view_form .social_row .you_tube, .view_form .social_row .fb{
			display:block;
			text-align:center;
			min-height: 50px;
			padding: 0;
		}

		.choose_one p{
			margin-bottom:25px;
		}

		.view_form .social_row .fb a{
			text-align:center;
		}

		.fb_public .fb_border {
			float:left;
		}

		.logo_row img{
			max-width:280px;
		}

	}

	@media only screen and (min-device-width : 480px) and (max-device-width : 767px) {
		.fb_public .fb_border {
			float: none;
		}
	}

	.fb_public .fb_border {
		padding:0
	}

	.fb_public .fb_border>.col-sm-12{
		padding:0;
	}

	.fb_public .heading span{
		color:#f19300
	}

	.fb_public .next_btn{
		color: #000000;
		padding: 6px 32px;
		font-size: 16px;
		float: right;
		margin: 15px 20px 15px 0;
		background:#f0ff00;
		text-transform:uppercase;
		font-family: "Open Sans",sans-serif;
	}

	.fb_public .next_btn:hover{
		background:#f0ff00;
	}

	.fb_public .back_btn{
		color: #000000;
		padding: 6px 32px;
		font-size: 16px;
		float: right;
		margin: 15px 20px 15px 0;
		background:#ffa200;
		text-transform:uppercase;
		font-family: "Open Sans",sans-serif;
	}

	.fb_public .back_btn:hover{
		background:#ffa200;
	}

	.fb_public img{
		max-width:100%
	}

	.youtube_official{
		color: #ff0000 !important;
	}

	#stream_description-error, #stream_title-error{
		color: red;
	}

	#bottom-help-img{
		z-index: 2147483646;
		width: 60px;
		height: 60px;
		font-size: 18px;
		font-weight: 400;
		overflow: hidden;
		justify-content: center;
		align-items: center;
		display: flex;
		cursor: pointer;
		position: fixed;
		right: 10px;
		bottom: 10px;
		
	}

    .col-form-comming-users{
        padding: 0 30%;
        text-align: left;
    }

    /*iPhone 6/7/8 */
    @media (max-width: 768px){

        /* Form Comming Users */
        .col-form-comming-users{
            padding: 0 15%;
        }

    }

</style>

<style>

	/** STYLE NEW INVITATION */


	/** CSS  */

	body{
		font-family: 'Roboto', sans-serif;

		background-image: url("http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2020/01/2303556048-BG.jpg");
		background-attachment: fixed !important;
		background-size: cover !important;
		background-position: top !important;
		background-repeat: no-repeat !important;
	}

	<?php if ( !empty($FormListingData['form_img1']) ){ ?>

		body{
			background-image: url(" <?=  $FormListingData->selected_image ?> ") !important;
		}

	<?php } ?>

	.flag:hover {
		text-decoration: none;
	}

	.img-zoom{
		transition: 0.5s transform ease;
	}

	.img-zoom:hover{
		transform: scale(1.1);
	}

	.btn-multiplier{
		color: black;
		border: 1px #00000036 solid;
		border-radius: 5px !important;
		padding: 5px 20px;
		transition: 0.3s ease background;
		cursor: pointer;
		margin: 0 10px;
	}

	.btn-multiplier:hover{
		background: #fcc946 !important;
	}

	.btn-green{
		color: white;
		background: #5daaa7 !important;
	}

	/** Css Header */

	#header_row{
		padding-bottom: 40px;
		position: relative;
	}

	#header_logo_wrapper img{
		width: 50%;
	}

	#flag_wrapper{
		position: absolute;
		right: 0;
		top: 50%;
		transform: translateY(-50%)
	}

	.view_form .flag_row{
		text-align: end;
	}

	.view_form .flag_row .flag img{
		height: 40px;
		width: 45px;
		border-radius: 20px !important;
		margin: 0 10px;
	}

	/** Css Details Text */

	.row.detail_text{
		position: relative;
		background: none !important;
		margin-bottom: 15%;
	}

	#convite_text_wrapper{
		background: white;
		border-radius: 20px !important;
		padding: 50px;
		float: right;
	}

	#invitation_headline_wrapper{
		position: absolute;
		left: 0;
		top: 50%;
		transform: translateY(-50%);
	}

	#invitation_headline{
		color: white;
		font-weight: bold;
		text-shadow: 0px 0px 5px #000;
	}

	#invitation_headline > p{
		font-size: 1.5em !important;
		text-align: left;
		line-height: 1.2;
		margin-left: 50px;
	}

	/** Css Social Row */

	.view_form .social_row{
		border: none;
		background: white;
		border-radius: 20px !important;
		padding: 70px 100px 20px 100px;
		margin-bottom: 15%;
	}

	#social_title{
		color: black;
		font-weight: bold;
	}

	#social_title > p{
		font-size: 1.5em !important;
		text-align: center;
		line-height: 1.2;
	}

	#social_title > p > b{
		font-weight: 600;
	}

	#social_warning{
		border: 1.5px solid #de0000;
		border-radius: 20px !important;
		padding: 20px;
		letter-spacing: 0.5px;
		line-height: 1.7;
		margin-bottom: 100px;
		text-align: left;
	}

	#col_social_text{
		padding-left: 0;
	}

	.col-form-comming-users{
		padding: 0;
	}

	.social_row > div{
		padding: 0 30px;
	}

	.choose_one p{
		text-align: left;
		font-weight: 500;
		line-height: 1.5;
		letter-spacing: 0.5px;
	}

	.logo_row hr{
		margin: 40px 0;
	}

	#share_text{
		float: left;
		margin-left: 60px;
	}

	#countryflag{
		padding-top: 0px;
		border: 1px solid #00000036;
	}

	.button-preview-copy{
		float: right;
		margin-right: 85px;
	}

	.you_tube{
		padding-left: 50px;
	}

	.view_form .social_row .fb_border img{
		max-width: 100%;
	}

	/** Css Footer */

	.view_form{
		padding: 0;
	}

	#footer_card{
		border: none;
		background: white;
		border-radius: 20px 20px 0 0 !important;
		padding: 50px 100px 10px 100px;
	}

	#footer_logo{
		width: 75%;
	}

	.footer-tag-img{
		width: 35%;
		vertical-align: middle;
		padding: 0 2%;
	}

	.footer-tag-img{
		text-decoration: none;
	}

	#footer_wrapper{
		position: relative; 
		margin-bottom: 20px;
	}

	#footer_login_wrapper{
		text-align: end;
		margin-top: 35px;
	}

	#footer_login_wrapper a:hover{
		color: white;
	}

	#footer_login_wrapper .btn{
		margin: 0 20px;
	}

	.knowMore_row{
		margin: 20px 0 40px 0;
	}

	#app_stores_wrapper{
		margin-bottom: 25px;
	}

	#app_stores{
		display: inline-block;
		width: 50%;
	}

	#text_rights{
		margin:0;
	}

	.knowMore_row a{
		color: black;
	}

	.knowMore_row a:hover{
		color: #5daaa7
	}

	#icons{
		text-align: end;
		margin-top: 20px;
	}

	#icons a{
		color: black;
		text-decoration: none;
	}

	#icons i{
		transition: 0.5s ease color;
		color: black;
		padding: 0 10px;
		font-size: 2em;
	}

	#icons i:hover{
		color: #5daaa7;
	}


	@media (max-width: 1024px){

		.button-preview-copy{
			margin: 0;
		}

		#app_stores{
			width: 70%;
		}

		#share_text{
			margin-left: 0;
		}

	}

	@media (max-width: 768px){

		#flag_wrapper{
			top: 0;
			transform: none;
		}

		#invitation_headline_wrapper{
			position: relative;
			right: inherit;
			top: inherit;
			transform: none;
			margin-bottom: 40px;	
		}

		#invitation_headline p {
			text-align: center;
			margin-left: 0px;
		}

		#social_warning{
			margin-bottom: 50px;
		}

		#footer_login_wrapper{
			text-align: center;
		}

		#share_text{
			margin-left: 0;
		}

		iframe{
			margin-bottom: 20px;
		}

		#icons{
			text-align: center;
			margin-top: 25px;
		}

		#app_stores{
			width: 80%;
		}
	}

	@media (max-width: 480px){
		
		#flag_wrapper{
			top: 0;
			transform: translateX(-50%);
    		padding: 0;
		}

		#header_logo_wrapper{
			margin-top: 50px;
		}

		#convite_text_wrapper{
			padding: 25px;
		}

		#invitation_headline_wrapper{
			position: relative;
			right: none;
			top: none;
			transform: none;	
		}

		#invitation_headline p {
			text-align: center;
			margin-left: 0px;
		}

		.view_form .social_row{
			padding: 25px;
		}

		.choose_one{
			padding: 0;
		}
		
		.social_row > div{
			padding: 0px;
		}

		#social_warning{
			font-size: 0.95em;
			padding: 20px;
			line-height: 1.5;
			margin-bottom: 15px;
		}

		#social_warning p {
			margin: 0;
		}

		.form-group .row > div{
			margin-bottom: 15px;
		}

		.row.logo_row{
			padding: 0;
		}

		.you_tube{
			padding: 15px;
		}

		.logo_row hr{
			margin: 20px 0;
		}

		#share_text{
			float: none;
		}

		.button-preview-copy{
			float: none;
		}

		#footer_card{
			padding: 20px;
		}

		#footer_login_wrapper .btn{
			margin: 0 10px;
		}

		#footer_logo{
			width: 100%;
		}

		#icons i {
			font-size: 1.6em;
		}

		iframe{
			height: 200px !important;
		}

		.knowMore_row{
			margin: 0px 0 40px 0;	
		}

		#app_stores{
			width: 100%;
		}

	}

</style>


<?php
$form_data = json_decode( $FormListingData['form_body']);
$form_data = json_decode( $form_data, true);
$session = $this->request->session();
$themeFa =  '<i class="fa fa-gift"></i>';
if($session->read( 'social.is_login' ) == "google"){
	$themeFa = '<i class="fa fa-youtube"></i>';
	$theme = $session->read('social.theme');
    }
$is_success_wizard=false;

// echo "<pre>";
// var_dump($FormListingData);
// exit();
?>

<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto&display=swap" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css" />

<div class="page-wrapper">
  <!-- BEGIN HEADER & CONTENT DIVIDER -->
  <div class="clearfix"> </div>
  <!-- END HEADER & CONTENT DIVIDER -->
  <!-- BEGIN CONTAINER -->
  <div class="page-container_dummy">
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
      <!-- BEGIN CONTENT BODY -->
      <div class="page-content view_form_page">
        <div class="container">
          <div class="row view_form">
		  <form role="form" class="form-horizontal" method="POST" id="form_sample_1">

            <div class="col-sm-12">

			<?php 
					$selectedLanguage='Portuguese';
					$gifImage='';
					if($session->read('language') == "en_US"){
						$selectedLanguage='English';
						$url = "../img/facebook_login.png";
						$yurl = "../img/you_tube.png";
						$gifImage = "../img/public_en.gif";
					}else if($session->read('language') == "en_SP"){
						$selectedLanguage='Spanish';
						$url = "../img/facebook_login.png";
						$yurl = "../img/you_tube.png";
						$gifImage = "../img/public_es.gif";
					}else{
						$selectedLanguage='Portuguese';
						$url = "../img/facebook_login.png";
						$yurl = "../img/you_tube.png";
						$gifImage = "../img/public_pt.gif";
					}     

					$text = ($form_description_lang ) ? $form_description_lang : __(causeDescritionText);
					$headline = ($form_title_lang ) ? $form_title_lang : __(causeTitle);
              	?>

              	<input type="hidden" name="form_lang" id="form_lang" value="<?= $selectedLanguage ?>"/>

              	<?php    
					if(!$session->read('successfull') && !isset($_GET['d'])){
					 	if ( ( empty( $session->read( 'fb.is_login' ) )  ||  $session->read( 'fb.is_login' ) == '' ) && ( empty( $session->read( 'social.is_login' ) )  ||  $session->read( 'social.is_login' ) == '' )){  

				?>
				
							<div class = "row" id = "header_row">

								<div class = "col-md-3" id = "header_logo_wrapper">
									<img class = "img-zoom" src = <?= $FormListingData->selected_header_image ?> >
								</div>

								<div class = "col-md-9" id = "flag_wrapper">

									<div class="row flag_row">
										<div> 
											
											<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=2" class="flag <?php echo ($session->read('language') == "en_BR")?'active':''; ?>" >
												<img class = "img-zoom" src="<?php echo '../img/brasilflag.jpg' ; ?>" / >
											</a> 

											<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=1" class="flag <?php echo ($session->read('language') == "en_US")?'active':''; ?>">
												<img class = "img-zoom" src="<?php echo '../img/usaflag.jpg' ; ?>" / >
											</a>

											<a href="<?php echo HTTP_ROOT ;?>app/changeLanguage?language=3" class="flag <?php echo ($session->read('language') == "en_SP")?'active':''; ?>">
												<img class = "img-zoom" src="<?php echo '../img/spanish.jpg' ; ?>" / > 
											</a> 

										</div>
									</div>

								</div>

							</div>
							
							<div class="row detail_text">

								<div class = "col-md-5" id = "invitation_headline_wrapper">
									<h2 id = "invitation_headline"><?= $headline?> </h2>
								</div>

								<div class = "col-md-7" id = "convite_text_wrapper">
									<p><?= $text?></p>

									<div id="initial-video" > 
										<video class = "<?= ($FormListingData->selected_video) ? "" : "hide" ?>" width="100%" height="100%" controls >
												<source src="<?php echo $FormListingData->selected_video; ?>" type="video/mp4">
										</video>
									</div>
								</div>

							</div>

					  		<div class="row social_row ">

								<div class = "col-md-6" id = "col_social_text">

									<div class = "col-md-12">
										<img class = "img-zoom" src = "../img/logo_big.jpg">
									</div>

									<div class="col-sm-12 choose_one">
										<p> <?= __(defaultPrivacyText) ?> <br> <?= __(formRegistrationPrivacy)?> </p>
									</div>

								</div>

								<div class = "col-md-6" id = "col_social_form"> 

									<div class="col-sm-12 col-form-comming-users">

										<form method="post" id="formsubmit" action="" class="formCommingUsers">

											<div class="form-group">
												<label class="control-label"><?php echo __(formRegistrationName); ?>*</label>
												<?php echo $this->Form->input('name', ['label' => false,'class'=>'form-control']); ?>
												<span id="form_error1" class="help-block help-block-error"></span>
											</div>

											<div class="form-group">
												<label class="control-label"><?php echo __(formRegistrationEmail); ?>*</label>
												<?php echo $this->Form->input('email', ['label' => false,'class'=>'form-control', 'type'=>'email']); ?>
												<span id="form_error1" class="help-block help-block-error"></span>
											</div>

											<div class="form-group">
												<label class="control-label"><?php echo __(formRegistrationMobile); ?>*</label>

												<div class="row">

													<div class="col-sm-3" style="clear: both;">
														<img id="countryflag" src="../img/flags/br.png" width="50"/>
													</div>

													<div class="col-sm-9">
														<select id="countries_dropdown" class="form-control" onChange="countyChanged()"> 

															<?php foreach($countries as $country){ 
																if(file_exists($_SERVER["DOCUMENT_ROOT"].'/app/webroot/img/flags/'.strtolower($country->iso2).'.png')){ ?>
																	<option value="<?php echo strtolower($country->iso2) ?>" data-contry-id="<?php echo $country->id; ?>" data-phone="<?php echo $country->phonecode; ?>" data-flag="<?php echo '/app/webroot/img/flags/'.strtolower($country->iso2).'.png';  ?>"><?php echo $country->name; ?></option>
															<?php }}?>

														</select>
													</div>

													<span id="form_error71" class="help-block help-block-error"></span>

												</div>

												<br/>

												<div class="row">
													<div class="col-sm-3" style="clear: both;">
														<input type="text" id="countrycode" class="form-control" value="+55" readonly />
													</div>

													<div class="col-sm-9">
														<?php echo $this->Form->input('phone', ['label' => false,'class'=>'form-control']); ?>
													</div>

													<span id="form_error7" class="help-block help-block-error"></span>
													
												</div>

											</div>

										</form>

									</div>

									<div class="col-sm-6 fb fb-login"> <a id="get_values_facebook" href="javascript:void(0);" ><img src="<?php echo $url; ?>"></a> </div>
									<div class="col-sm-6 you_tube"> <a  href="javascript:void(0);" id="get_values_gmail"> <img src="<?php echo $yurl;?>"></a> </div> 

								</div>

								<div class="row fb_public fb_login_box" style="display:none">
									<div class="col-sm-12 fb_border">

										<div class="col-sm-12">
											<p class="heading"><?php echo __(social_select2);?></p>
										</div>

										<div class="col-sm-12">
											<img src="<?php echo $gifImage; ?>" >
										</div>

										<div class="col-sm-12">
											<button type="button" class="btn back_btn" style="float:left;margin-left:10px;"  onclick="$('.fb_login_box').hide();$('#col_social_text, #col_social_form, .flag_row, .img_row').show();">  <i class=" fa fa-angle-left"></i> <?php echo __(back);?>  </button>
											<button type="button" class="btn next_btn"  id="get_values"><?php echo __(next);?>  <i class=" fa fa-angle-right"></i></button>
										</div>  

									</div>                                         
					   			</div>

								<div class="row logo_row">

									<div class="col-sm-12"> 

										<?php if ( $_GET['id']!=2507){ ?>

											<hr>
											<h5 id = "share_text"> <b> <?php echo __(ShareThisInvitationToYourFriends); ?> </b> </h5>
											<div class="button-preview-copy ">
												<div class="sharethis-inline-share-buttons"
													data-title="<?= $headline ?>" 
													data-url="<?php  echo HTTP_ROOT; ?>commingusers/view-form?id=<?php echo $FormListingData['form_id']; ?>" 
													data-description="<?= $text ?>" 
													data-image="<?php echo HTTP_ROOT.'img/uploads/'.(@$siteinfo->site_logo != ''?@$siteinfo->site_logo:'logo.png'); ?>"></div>
									
											</div>
										<?php } ?>

									</div>

								</div>

            				</div>

              			<?php } else{ ?>

							<input type="hidden" value="<?php echo isset($new_stream_url_secure)? $new_stream_url_secure:''; ?>" name="new_stream_url_secure">
							<input type="hidden" value="" name="json_data">

              				<?php if($session->read( 'social.is_login' ) == "google"){

								$name           =  $session->read( 'social.name' );
								$identifier     =  $session->read( 'social.identifier' );
								$profile_url    = $session->read( 'social.profile_url' );
								$image          =  $session->read( 'social.image' );
								$email          = $session->read( 'social.email' ); 
								$access_token = $session->read('social.accesstoken');
								$logOutUrl = HTTP_ROOT."commingusers/view-form?id=".$_GET['id'].'&logout=1';
								$targname = "gmail";
								
							?>

							<div class="row fb_login_box youtube_login_box">

								<div class="col-sm-12 fb_border">

									<p class="heading"><?php echo __(register_youtube); ?></p>

									<div class="login_form">

										<div class="row flex_row fb_user_row">

											<div class="col-sm-2 col-sm-offset-2"> <img src="<?php echo $image; ?>"> </div>

												<div class="col-sm-5">
													<p><strong><?php echo $name; ?> </strong> <?php echo __(register_youtube2); ?></p>
												</div>

											</div>

											<div class="row">

												<div class="col-sm-3 align-right">
													<label for="email" class="select_reg"><?php echo __(permission); ?>*</label>
												</div>

												<div class="col-sm-8">

													<div class="form-group">
														<select class="form-control" id="privacy" name="privacy"  placeholder="Input">
															<option value="public"><?php echo __(publics);?></option>
														</select>
													</div>

												</div>

											</div>

											<div class="row">

												<div class="col-sm-3 align-right">
													<label for="email" class="select_reg"></label>
												</div>

												<div class="col-sm-8">

													<div class="checkbox fb_checkbox">
														<label>
														<input type="checkbox" name="agree" value="1" required="required">
														&nbsp; <?php echo __(iagree); ?> <a target="_blank" href="<?php echo HTTP_ROOT.'terms' ?>"><?php echo __(terms); ?></a><?php echo " ".__(andmsg)." "; ?><a target="_blank" href="<?php echo HTTP_ROOT.__(url_cookies_lang) ?>"><?php echo __(private_policy); ?></a></label>
													</div>

												</div>

											</div>

											<div class="row button_row">
												<div class="col-sm-12"> <a href="<?php echo $logOutUrl?>" id="change_user" class="btn logout next_btn"><i class="fa fa-sign-out"></i> <?php echo __(logout);?> </a> <button type="submit" class="btn register next_btn"><?php echo __(REGISTER);?> <i class=" fa fa-angle-double-right"></i></button> </div>
											</div>

										</div>

									</div>

								</div>

              				<?php }else{

								$targname = "facebook";
								$name           =  $session->read( 'fb.name' );
								$identifier     =  $session->read( 'fb.identifier' );
								$profile_url    = $session->read( 'fb.profile_url' );
								$image          =  $session->read( 'fb.image' );
								$email          = $session->read( 'fb.email' ); 
								$access_token = $session->read('fb.accesstoken');
								$logOutUrl = "https://www.facebook.com/logout.php?next=".HTTP_ROOT."commingusers/view-form?id=".$_GET['id']."&logout=1&access_token=".@$access_token;
								$posts  = $session->read( 'fb.posts' );
								$pages  = $session->read( 'fb.accounts' );
								$groups = $session->read( 'fb.groups' );
								$events  = $session->read( 'fb.events' );

                                $newEventsArr = array("data"=>array());
                                foreach($events['data'] as $e){
                                    $status = @$this->Custom->checkTargetsEndTime($e['id'], 'event', $access_token);
                                    if($status){
                                        array_push($newEventsArr['data'], $e);
                                    }
                                }
									
								$target_name = $app_name = 'mobilyte'  . '_FB_' . rand(0,9999) . time();
								
							
							?> 

                            <?php   if($FormListingData['hide_share_option'] ==  "0"){ ?> 

								<div id="social_step_1">

									<div class="row fb_login_box">

									  	<div class="col-sm-12 fb_border">

											<p class="heading"><?php echo __(social_select4);?></p>

											<div class="login_form">
									
										  		<p><i class="fa fa-edit"></i></p>

										  		<div class="row">

													<div class="col-sm-6 col-sm-offset-3">

											  			<p><strong>Multiplier</strong> <?php echo __(social_select5);?></p>

											  			<div class="form-group">

															<select class="form-control" placeholder="Input" id="share" name="share">
																<option value="" readonly><?php echo __(select); ?></option>
																<option value="SELF"><?php echo __(Onlyme); ?></option>
																<option value="ALL_FRIENDS"><?php echo __(friends); ?></option>
																<option value="FRIENDS_OF_FRIENDS"><?php echo __(FRIENDS_OF_FRIENDS); ?></option>
																<option value="EVERYONE"><?php echo __(publics); ?></option>
															</select>

															<span id="shareOptionRequired" style="display:none;color:red;"><?php echo __(Required);?></span>

											  			</div>

													</div>
										  		</div>

										  		<div class="row button_row">
													<div class="col-sm-12"> <a href="javascript:void(0);" onClick="fbShareOptionSelect()" class="btn next_btn"><?php echo __(next);?> <i class="fa fa-angle-right"></i></a> </div>
										  		</div>

                      							<?php if($session->read('social_selected_error')){ ?>

                      								<div class="alert alert-danger"><?php echo $session->read('social_selected_error'); 
                         							$session->write('social_selected_error',"");
                      								?></div>

                      							<?php } ?>

											</div>

                                        </div>

                                	</div>

                                </div>

                            <?php  } ?>

              				<input id="share" name="share"  type="hidden" value="EVERYONE">

								  <div id="social_step_<?php echo ($FormListingData['hide_share_option']=="0")?2:1;?>" style="<?php echo ($FormListingData['hide_share_option']=="0")?'display:none':'';?>">
									<div class="row fb_login_box">
									  <div class="col-sm-12  fb_border">
										<p class="heading"><?php echo __(social_select6); ?></p>
										<div class="login_form">
										  <div class="row flex_row fb_user_row">
											<div class="col-sm-2 col-sm-offset-2"> <img src="<?php echo $image;?>"> </div>
											<div class="col-sm-5">
											  <p><strong><?php echo ucwords($name);?> </strong> <?php echo __(not_facebook); ?></p>
											</div>
										  </div>
										  <div class="row">
											<div class="col-sm-3 align-right">
											  <label for="email" class="select_reg"><?php echo __(Selectdestination); ?>:</label>
											</div>
											<div class="col-sm-8">
											  <div class="form-group">
												<select style="" class="form-control" name="flag" id="flag" placeholder="Input">
												  <!--<option value="" readonly><?php echo __(cao); ?></option>-->
												  <option value="timeline" ><?php echo __(yourfacebook); ?></option>
												  <option value="page"><?php echo __(YourPages); ?></option>
												  <option value="group"><?php echo __(YourGroup); ?></option>
												  <option value="event"><?php echo __(YourEvent); ?></option>
												</select>
											  </div>
											</div>
										  </div>
										  <div class="row"  id="stream_title" style="display:none;">
											<div class="col-sm-3 align-right">
											  <label for="email" class="select_reg"><?php echo __(Streamtargetname); ?>*</label>
											</div>
											<div class="col-sm-8">
											  <div class="form-group">
												<input class="form-control" name="target_name" type="text" value="<?php echo $target_name;?>" />
											  </div>
											</div>
										  </div>
										  
										  <div class="row"  id="show_pages" style="display:none;">
											<div class="col-sm-3 align-right">
											  <label for="email" class="select_reg"><?php echo __(Mypages); ?>*</label>
											</div>
											<div class="col-sm-8">
											  <div class="form-group">
                                            <select class="form-control" id="pages" name="pages"  placeholder="Input">
                                            <option value="" readonly><?php echo __(select); ?></option>
													<?php foreach($pages['data'] as $p){ ?>
                                                <option value="<?php echo $p['id']; ?>"><?php echo $p['name']; ?></option>
													<?php } ?>
                                            </select>
                                        </div>
                                </div>
										  </div>
										  <div class="row"  id="show_groups" style="display:none;">
											<div class="col-sm-3 align-right">
											  <label for="email" class="select_reg"><?php echo __(Mygroup); ?>*</label>
											</div>
											<div class="col-sm-8">
											  <div class="form-group">
                                            <select class="form-control" id="groups" name="groups"  placeholder="Input">
                                            <option value="" readonly><?php echo __(select); ?></option>
												  <?php foreach($groups['data'] as $g){ ?>
					
					
                                                <option value="<?php echo $g['id']; ?>"><?php echo $g['name']; ?></option>
												  <?php } ?>
                                            </select>
                                        <p style="padding: 5px; margin: 5px; font-size: 13px;"><?php echo __(group_warning); ?></p>
                                        </div>
                                </div>
										  </div>
										  <div class="row"  id="show_events" style="display:none;">
											<div class="col-sm-3 align-right">
											  <label for="email" class="select_reg"><?php echo __(Myevent); ?>*</label>
											</div>
											<div class="col-sm-8">
											  <div class="form-group">
                                            <select class="form-control" id="events" name="events"  placeholder="Input">
                                            <option value="" readonly><?php echo __(select); ?></option>
                                            <?php foreach($newEventsArr['data'] as $e){ ?>
                                                <option value="<?php echo $e['id']; ?>"><?php echo $e['name']; ?></option>
                                            <?php } ?>
                                            </select>
												<p style="padding: 5px; margin: 5px; font-size: 13px;"><?php echo __(event_warning); ?></p>
											  </div>
                                        </div>
                                </div>
                                <?php if($FormListingData['fb_td_status'] ==  "0"){ ?>       
										  <div class="row">
											<div class="col-sm-3 align-right">
											  <label for="email" class="select_reg"><?php echo __(title); ?>*</label>
											</div>
											<div class="col-sm-8">
                                <div class="form-group">        
												<input class="form-control" name="stream_title" placeholder="" type="text" >
                                        </div>
                                </div>
										  </div>
										  <div class="row">
											<div class="col-sm-3 align-right">
											  <label for="email" class="select_reg"><?php echo __(description); ?>*</label>
											</div>
											<div class="col-sm-8">
                                <div class="form-group">        
												<input class="form-control" name="stream_description" placeholder="" type="text" >
											  </div>
                                        </div>
                                </div>
                           <?php
																   }else{ ?>
										  <input name="stream_title" type="hidden" value="<?php echo $FormListingData['facebook_title'];?> ">
											<?php //echo str_replace('"',"'",$FormListingData['facebook_description']);?>
										  <input name="stream_description" type="hidden" value="<?php echo str_replace('"',"'",$FormListingData['facebook_description']);?>">
										  <?php }?>
										   <div class="row">
											<div class="col-sm-3 align-right">
											  <label for="email" class="select_reg"></label>
                                </div>
											<div class="col-sm-8">
											  <div class="checkbox fb_checkbox">
												<label>
												<input type="checkbox" name="agree" value="1" required="required">
												&nbsp; <?php echo __(iagree); ?> <a target="_blank" href="<?php echo HTTP_ROOT.'terms' ?>"><?php echo __(terms); ?></a><?php echo " ".__(andmsg)." "; ?><a target="_blank" href="<?php echo HTTP_ROOT.__(url_cookies_lang) ?>"><?php echo __(private_policy); ?></a></label>
                        </div>
                                        </div>
                                </div>

										  <div class="row button_row">
											<div class="col-sm-12"> <a href="<?php echo $logOutUrl; ?>" id="change_user" class="btn logout next_btn"><i class="fa fa-sign-out"></i> <?php echo __(logout); ?> </a> <button type="submit"  class="btn register next_btn"><?php echo __(REGISTER); ?> <i class=" fa fa-angle-double-right"></i></button> </div>
										  </div>
                            <input type="hidden" id="CommingUsersCount" value="<?php echo $CommingUsersCount; ?>">
										  <?php if($session->read('social_selected_error')){ ?>
											<div class="alert alert-danger"><?php echo $session->read('social_selected_error'); 
												 $session->write('social_selected_error',"");
											?></div>
											<?php } ?>
                                                
                                        </div>                                                        
                                    </div>
                                </div>
								  </div>
					  <?php   } ?>
					  <?php }?>
					  <?php }else{
								$is_success_wizard=true;
								if($session->read( 'social.is_login' ) == "google"){?>
                                                
								<div class="row fb_login_box youtube_sucess">
									<div class="col-sm-12 fb_border">
										<p class="heading"><?php echo __(youtube_registered_successfully); ?> </p>
									 <div class="login_form">          
									 <p style="text-transform: none;"><?php echo __(NowWhatDoYouWantToDo); ?></p>
									  <div class="row other_register">
									   <div class="col-sm-4 youtube">
										  <a href="javascript:void(0);" id="get_values_gmail"><?php echo __(register_another_youtube); ?></a>
                                        </div>                                                        
									   <div class="col-sm-4 untill_now">
										  <a href="javascript:void(0);" onclick="$('#socialnetworkList').show()"><?php echo __(SeeAllRegisteredUntilNow); ?></a>
                                    </div>
									   <div class="col-sm-4 facebook">
										  <a href="javascript:void(0);" id="get_values"><?php echo __(register_another_facebook); ?></a>
                                    </div>
                                </div>
                        </div>
</div>                              
								   </div>
							<?php }else{?>
									<div class="row fb_login_box youtube_sucess facebook_sucess">
									<div class="col-sm-12 fb_border">
										<p class="heading"><?php echo __(facebook_registered_successfully); ?> </p>
									 <div class="login_form">          
									 <p style="text-transform: none;"><?php echo __(NowWhatDoYouWantToDo); ?></p>
									  <div class="row other_register">
										 <div class="col-sm-4 facebook">
										  <a href="javascript:void(0);"  id="get_values"><?php echo __(register_another_facebook); ?></a>
									   </div>
									   <div class="col-sm-4 untill_now">
										  <a href="javascript:void(0);" onclick="$('#socialnetworkList').show()"><?php echo __(SeeAllRegisteredUntilNow); ?></a>

								   </div>
										<div class="col-sm-4 youtube">
										  <a href="javascript:void(0);"  id="get_values_gmail"><?php echo __(register_another_youtube); ?></a>
									   </div>
									  </div>
									  </div>
									</div>
								   </div>

						<?php }
							$session->write('successfull',""); 
					 }?> 
					  <?php if(!empty($fbUserSubData)){?>
					  <div class="row table_row" id="socialnetworkList"  style="<?php echo ($is_success_wizard==true)?'display:none':'';?>">
						<div class="col-sm-12">
						  <p><?php echo __(social_select3); ?></p>
						  <div class="table-responsive table_data">
							<table class="table">
							  <thead>
                                <tr>
                                    <th><?php echo __(source); ?></th>
                                    <th><?php echo __(image); ?></th>
                                    <th><?php echo __(Destination); ?></th>
                                    <th><?php echo __(name); ?></th>
                                    <th colspan="2" width="10%" class="text-left"><?php echo __(delete); ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php 
                                    foreach ($fbUserSubData as $fbData) 
                                    { 
                                        $fbData->image_path = $fbData->image_path ? $fbData->image_path : HTTP_ROOT.'img/dummy_image.png';

                                        if($fbData->flag == 'timeline'){ $flag = 'Seu Facebook'; }
                                        if($fbData->flag == 'page'){ $flag = 'Página'; }
                                        if($fbData->flag == 'group'){ $flag = 'Grupo'; }
                                        if($fbData->flag == 'event'){ $flag = 'Evento'; }
                                        if($fbData->flag == "youtube"){ $flag = 'YouTube'; }
                                ?>
                                <tr>
                                    <?php if($fbData->flag != "youtube") { ?>
								  <td><img src="../img/facebook.svg"/></td>
                                     <?php } else { ?>
								  <td><i class="youtube_official fa fa-youtube fbIcon" aria-hidden="true" data-text="Youtube"></i></td>
                                     <?php } ?>
                                    <td><img class="listing_image" src="<?php echo $fbData->image_path;?>"></td>
                                    <td><?php echo $flag;?></td>
                                    <td><?php echo ucfirst( $fbData->fb_name );?></td>
                                    <?php if($FormListingData['fb_td_status'] ==  "0"){ ?>   
								  <td align="center"><a  onclick = "return confirm('Are you sure?')" href="<?php echo HTTP_ROOT; ?>commingusers/edit-common?id=<?php echo $fbData->id?>&fid=<?php echo $fbData->form_id?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a> </td>
                                    <?php } ?>
								  <td><a  href="<?php echo HTTP_ROOT; ?>commingusers/deletestream/?id=<?php echo $fbData->id?>&eid=<?php echo $fbData->fb_email?>&fid=<?php echo $fbData->form_id?>" onClick="return confirm('Are you sure?')" class="delete"><i class="fa fa-trash"></i></a></td>
                                </tr>
                            <?php  }?>  
                            </tbody>
                        </table>
                </div>
                </div>
            </div>
            <?php }?>

			<div id = "footer_card">
				
				<?php if ( $_GET['id']!=2507){ ?> 

					<div class = "row" id = "footer_wrapper">

						<div class = "col-md-6">
							<p><a href="https://multiplierapp.com.br/"> <img class = "img-zoom" id = "footer_logo" src="../img/logo_big.jpg"> </a> </p>
						</div>

						<div class="col-md-5" id = "footer_login_wrapper">
							<a class = "btn btn-multiplier" href = "https://multiplierapp.com.br/politicas-de-uso/"> Criar Conta </a>
							<a class = "btn btn-multiplier btn-green" href = "https://multiplierapp.com.br/app/users/login"> Login </a>
						</div>

					</div>

				<?php } ?>

				<?php if (!empty($videoId) && $_GET['id']!=2507){ ?>

					<div class="row advertising_row">

						<div class="col-md-6"> 

							<?php if($videoType == 'youtube'){ ?>
								<iframe width="100%" height="315" src="https://www.youtube.com/embed/<?php echo $videoId; ?>" frameborder="0" allowfullscreen></iframe>
							<?php }else{ ?>
								<iframe src="https://player.vimeo.com/video/<?php echo $videoId; ?>" width="100%" height="315" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
							<?php } ?> 

						</div>

						<div class = "col-md-6">
							<iframe width="100%" height="315" id="youtube_video" src="https://www.youtube.com/embed/Z4j3bwCHKqo?feature=oembed&amp;start&amp;end&amp;wmode=opaque&amp;loop=0&amp;controls=1&amp;mute=0&amp;rel=0&amp;modestbranding=0"> </iframe>
						</div>

					</div>
					
				<?php } ?>
							
				<?php if ( $_GET['id']!=2507){ ?>

					<div class="row knowMore_row">

						<div class="col-md-6">
							<p> <?php echo __(KnowMoreAboutUsAt); ?> <a href="http://www.multiplierapp.com.br/" target="_blank">www.multiplierapp.com.br</a></p>
						</div>

						<div class = "col-md-5" id = "icons">

							<a href = "https://www.instagram.com/multiplierapp/"> <i class="fab fa-instagram img-zoom"></i> </a>
							<a href = "https://www.facebook.com/multiplierapp/"> <i class="fab fa-facebook-f img-zoom"></i> </a>
							<a href = "https://www.youtube.com/c/MultiplierApp"> <i class="fab fa-youtube img-zoom"></i> </a>
							<a href = "https://twitter.com/MultiplierApp"> <i class="fab fa-twitter img-zoom"></i> </a>
							<a href = "https://www.linkedin.com/company/multiplierapp/"> <i class="fab fa-linkedin-in img-zoom"></i> </a>

						</div>

					</div>

				<?php } ?>

				<div class = "row">
					
					<div class = "col-md-12" id = "app_stores_wrapper">
						<div id = "app_stores">
							<a href = "https://play.google.com/store/apps/details?id=com.br.multiplierapp"> <img class = "img-zoom footer-tag-img" src = "http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2019/11/Multiplierapp-Baixe-no-google-play.png"> </a>
							<a href = "https://apps.apple.com/app/multiplierapp/id1301426934?ls=1"> <img class = "img-zoom footer-tag-img" src = "http://www.multiplierapp.fcdesigner.art.br/wp-content/uploads/2019/11/Multiplierapp-Baixe-no-apple-store.png"> </a>
						</div>
					</div>

				</div>

				<div class = "row">

					<div class = "col-md-12">
						<p id = "text_rights">@2020 MultiplierApp – Todos os direitos reservados / All rights reserved / Todos los derechos reservados.</p>
					</div>

				</div>

			</div>

            </div>
			</form>
          </div>
        </div>
      </div>
      <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
  </div>
  <!-- END CONTAINER -->
</div>

<script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>                 
<script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="//platform-api.sharethis.com/js/sharethis.js#property=5b362317c5ed9600115214d3&product=inline-share-buttons"></script>
<script type="text/javascript"> var base_url = '<?php echo HTTP_ROOT; ?>';</script>


<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/inputmask.min.js" integrity="sha256-OeQqhQmzxOCcKP931DUn3SSrXy2hldqf21L920TQ+SM=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/inputmask.extensions.min.js" integrity="sha256-vpL5m6IilsG6TXFKZ99NU+cBmT122RJ6sqBgq0a9/vQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/inputmask/4.0.9/inputmask/jquery.inputmask.min.js" integrity="sha256-x4dnYQ/ZWD3+ubQ1kR3oscEWXKZj/gkbZIhGB//kmmg=" crossorigin="anonymous"></script>

<?php //pr($form_data);?>

<!-- BEGIN THEME LAYOUT STYLES -->
<script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
    $(document).on('click', 'helptext', function (event) {
      $('[data-toggle="tooltip"]').tooltip(); 
    });
</script>

<script type="text/javascript">


    //DEV-116 - Get mandatory aditional data at registration forms
    function countyChanged(){
        var contry_flag = $("#countries_dropdown option:selected").attr("data-flag");
        $('#countryflag').attr('src', contry_flag);

        var country_code = $("#countries_dropdown option:selected").attr("data-phone");
        $('#countrycode').val(country_code);

        var country_id = $("#countries_dropdown option:selected").attr("data-contry-id");

        var states = $("#state");

        if (country_code == '55'){

            $('#phone').inputmask('(99) 99999-9999',{ "clearIncomplete": true });

        }else{
            $('#phone').inputmask('');
        }


    }




    function validateForm(){

        var name = $('#name').val();
        var email = $('#email').val();
        var country = $('#countries_dropdown').val();
        var phone = $('#phone').val();


        if (name == '' || email == '' || country == '' || !validateBRPhone(phone) || !validateEmail(email)){
            return false;
        }else{
            return true;
        }

    }


    function  validateEmail($email) {
        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
        return emailReg.test( $email );
    }

    function validateBRPhone($phone) {
        var phoneReg = /(\([1-9][0-9]\)?|[1-9][0-9])\s?([9]{1})([0-9]{4})-?([0-9]{4})$/;
        return phoneReg.test( $phone );
    }

			function fbShareOptionSelect(){
							if($("#share").val()==""){
									$("#shareOptionRequired").show();
											return false;
							}
							$("#shareOptionRequired").hide();
							$('#social_step_1').hide();$('#social_step_2').show();
			}
	
    var from_data = <?php echo json_encode($form_data) ?>;
    // console.log(from_data);
    var validation_fields = {};
        jQuery.each(from_data, function(i, field)
        {
           if( typeof field.required !== 'undefined' )
           {
                var jsonObj = {};
                if( typeof field.subtype !== 'undefined' )
                {   
                    if( field.subtype == 'email' )
                    {
                        jsonObj['email'] = true;
                    }

                    if( field.subtype == 'tel' )
                    {
                        jsonObj['number'] = '!0';
                        jsonObj['minlength'] = '5';
                        jsonObj['maxlength'] = '10';
                    }
                }
                jsonObj['required'] = true;
                validation_fields[field.name] = jsonObj;
            }
        });

        var flagObj = {};
        flagObj['required'] = true;
        validation_fields['flag'] = flagObj;


        var streamTitleObj = {};
        streamTitleObj['required'] = true;
        validation_fields['stream_title'] = streamTitleObj;


        var streamDescriptionObj = {};
        streamDescriptionObj['required'] = true;
        validation_fields['stream_description'] = streamDescriptionObj;
                
                // console.log(validation_fields);
    var FormValidation = function() {
    var e = function() {
   
    var formLang = $("#form_lang").val();
    //alert(formLang);

      var validMsg = '';
      if(formLang == 'Portuguese')
      {
        validMsg = "Os campos Título e Descrição do post não podem ficar em branco.";
      }
      else if(formLang == 'English')
      {
        validMsg = "Post Title and Description fields must not be blank.";
      }
      else if(formLang == 'Spanish')
      {
        validMsg = "Los campos Título y Descripción de la publicación no deben estar en blanco.";
      }
            var e = $("#form_sample_1"),
                r = $(".alert-danger", e),
                i = $(".alert-success", e);
            e.validate({
                errorElement: "span",
                errorClass: "help-block",
                errorPlacement: function(error, element) {
                    // console.log(element);
                    if( ( element.parent('.mt-radio').length ) || ( element.parent('.mt-checkbox').length) ) {
                        error.insertAfter(element.parent().parent());
                    } else {
                        error.insertAfter(element);
                    }
                },
                focusInvalid: !1,
                ignore: "",
                messages: {
                    select_multi: {
                        maxlength: jQuery.validator.format("Max {0} items allowed for selection"),
                        minlength: jQuery.validator.format("At least {0} items must be selected")
                    },
                    stream_title: validMsg,
                    stream_description: validMsg,
                },
                rules: validation_fields,
                // ,rules: {
                //     name: {
                //         minlength: 2,
                //         required: !0
                //     },
                //     email: {
                //         required: !0,
                //         email: !0
                //     },
                // },
                invalidHandler: function(e, t) {
                    //i.hide(), r.show(), App.scrollTo(r, -200)
                },
                highlight: function(e) {
                    $(e).closest(".form-group").addClass("has-error")
                },
                unhighlight: function(e) {
                    $(e).closest(".form-group").removeClass("has-error")
                },
                success: function(e) {
                    e.closest(".form-group").removeClass("has-error")
                },
                submitHandler: function(e) {
                    return true;
                   // i.show(), r.hide()
                }
            });


            jQuery('#flag').change(function(){
                if( jQuery(this).val() == 'timeline' )
                {
                    jQuery("#share").rules("add", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");

                }
                else if( jQuery(this).val() == 'page' )
                {
                    jQuery("#pages").rules("add", "required");
                    jQuery("#share").rules("remove", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");
                }
                else if( jQuery(this).val() == 'group' )
                {
                    jQuery("#groups").rules("add", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#share").rules("remove", "required");
                    jQuery("#events").rules("remove", "required");
                }
                else if( jQuery(this).val() == 'event' ){
                    jQuery("#events").rules("add", "required");
                    jQuery("#groups").rules("remove", "required");
                    jQuery("#pages").rules("remove", "required");
                    jQuery("#share").rules("remove", "required");
                }
                else
                {

                }
            });

        };
    return {
        init: function() {
            e() // t(), r(), i()
        }
    }
}();
	jQuery(document).ready(function() {


        //DEV-116 - Get mandatory aditional data at registration forms
        $('#countries_dropdown').val('br'); 
        $('#countries_dropdown').trigger('change'); 

    $('#change_user').click(function(event){
        event.preventDefault();
        var urlLogout = base_url + 'commingusers/fblogout/?id=';

        <?php  if($session->read( 'social.is_login' ) == "google"){ ?>
            urlLogout = base_url + 'commingusers/commonLogOut/?id=';
        <?php } ?>
        var logout_url =  $(this).attr('href');
        $.ajax({
            'url' : urlLogout + <?php echo $_GET['id'];?>,
            success: function(data){
               window.location.href = logout_url;
            }
        });

    });
    $('#flag').change(function(){
        var flag = $('#flag').val();
        if(flag == 'timeline'){
           
            var hide_share_option = '<?php echo $FormListingData['hide_share_option'];?>';
            if( hide_share_option == "0"){
               $('#show_timeline').show();
            }
            $('#stream_title').hide();
            $('#show_pages').hide();
            $('#show_events').hide();
            $('#show_groups').hide();
        } else if(flag == 'page') {
            $('#show_pages').show();
            $('#show_events').hide();
            $('#stream_title').hide();
            $('#show_timeline').hide();
            $('#show_groups').hide();
        } else if(flag == 'group') {
            $('#show_groups').show();
            $('#show_events').hide();
            $('#show_pages').hide();
            $('#stream_title').hide();
            $('#show_timeline').hide();
        }else if(flag == 'event'){
            $('#show_events').show();
            $('#show_groups').hide();
            $('#show_pages').hide();
            $('#stream_title').hide();
            $('#show_timeline').hide();
        }else{
            $('#show_pages').hide();
            $('#show_events').hide();
            $('#show_timeline').hide();
            $('#stream_title').hide();
            $('#show_groups').hide();
            return false;
        }
    });
    $('#get_values').click(function(){
        window.location.href = base_url + 'commingusers/fblogin?id=' + <?php echo $_GET['id'];?>; 
    });

    $('#get_values_gmail').click(function(){

            if (validateForm() == true){

                //Get values from form to session (AJAX)
                var name_value = $('#name').val();
                var email_value = $('#email').val();
                var country_value = $('#countries_dropdown option:selected').text();
                var phone_value = $('#phone').val();
                var state_value = $('#state option:selected').text();
                var city_value = $('#city option:selected').text();


                $.ajax({
                    url: "/app/commingusers/saveFormSession",
                    type: "POST",
                    data: {name: name_value, email: email_value, country: country_value, phone: phone_value, state: state_value, city: city_value },
                    dataType: "json"
                }).done(function(result) {

                });


        window.location.href = base_url + 'commingusers/glogin?id=' + <?php echo $_GET['id'];?>; 
            }else{

                var formLang = $("#form_lang").val();
                if(validateBRPhone(phone))
                {
                    if(formLang == 'Portuguese')
                {
                    var validMsg = "Todos os campos são de preenchimento obrigatório.";
                }
                else if(formLang == 'English')
                {
                    var validMsg = "All fields are required.";
                }
                else if(formLang == 'Spanish')
                {
                    var validMsg = "Todos los campos son obligatorios.";
                }
                alert(validMsg);

               }
               else
               {
                if(formLang == 'Portuguese')
                {
                    var validMsg = "Número de telefone móvel inválido.";
                }
                else if(formLang == 'English')
                {
                    var validMsg = "Invalid mobile phone number.";
                }
                else if(formLang == 'Spanish')
                {
                    var validMsg = "Número de teléfono móvil no válido.";
                }
                $('#phone').blur();
                alert(validMsg);

               }
           }
        });


        $('#get_values_facebook').click(function(){

            if (validateForm() == true){

                //Get values from form to session (AJAX)
                var name_value = $('#name').val();
                var email_value = $('#email').val();
                var country_value = $('#countries_dropdown option:selected').text();
                var phone_value = $('#phone').val();



                $.ajax({
                    url: "/app/commingusers/saveFormSession",
                    type: "POST",
                    data: {name: name_value, email: email_value, country: country_value, phone: phone_value },
                    dataType: "json"
                }).done(function(result) {

                });


                $('.fb_login_box').show();
                $('#col_social_text, #col_social_form, .flag_row, .img_row').hide();
                // $('html, body').animate({ scrollTop: 0 }, 'slow');



            }else{

var formLang = $("#form_lang").val();
if(validateBRPhone(phone))
{
    if(formLang == 'Portuguese')
{
    var validMsg = "Todos os campos são de preenchimento obrigatório.";
}
else if(formLang == 'English')
{
    var validMsg = "All fields are required.";
}
else if(formLang == 'Spanish')
{
    var validMsg = "Todos los campos son obligatorios.";
}
alert(validMsg);

}
else
{
if(formLang == 'Portuguese')
{
    var validMsg = "Número de telefone móvel inválido.";
}
else if(formLang == 'English')
{
    var validMsg = "Invalid mobile phone number.";
}
else if(formLang == 'Spanish')
{
    var validMsg = "Número de teléfono móvil no válido.";
}
alert(validMsg);

}
}
    });
   
	FormValidation.init();
   
});


</script>
<script type="text/javascript">
$(document).on('change', '.other', function (event) {
    if($(this).is(':checked')){
        $(".other-box").show(200);
        $(".other-box").prop('required',true);
        // $(this).parent().find(".help-block").show();
    }else{
        $(".other-box").hide(200);
        // $(this).parent().find(".help-block").hide();
        $(".other-box").prop('required',false);
    }
});
$(document).on('keyup', '.other-box', function (event) {
    $(".other").val($(this).val());
});


$(document).on("click", "input[class='radio-input']", function (event) 
{
    var is_other = $(this).attr('other-field');
    console.log($(this).attr('other-field'));
    console.log($(this));
    if(is_other == '1'){
        $(".other-box-radio").show(200);
        $(".other-box-radio").prop('required',true);
        // $(this).parent().find(".help-block").show();
    }else{
        $(".other-box-radio").hide(200);
        // $(this).parent().find(".help-block").hide();
        $(".other-box-radio").prop('required',false);
    }

});
$(document).on('keyup', '.other-box-radio', function (event) {
    $(".radio-input").val($(this).val());
});

</script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-150918709-1"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());

gtag('config', 'UA-150918709-1');
</script>
<!--Start of Tawk.to Script-->
<!-- <script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/58481b7a8a20fc0cac4e46a6/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script> -->
<!--End of Tawk.to Script-->
<!--<link href="https://snatchbot.me/sdk/webchat.css" rel="stylesheet" type="text/css"><script src="https://snatchbot.me/sdk/webchat.min.js"></script><script> Init('?botID=22104&appID=UWHbOGdOy8LpXhyFH5zy', 400, 400, 'https://dvgpba5hywmpo.cloudfront.net/media/image/rywfP58bGrSVFGkGopRBWsOrB', 'bubble', '#53A19A', 90, 90, 62.99999999999999, '', '1', '#FFFFFF', 0);  </script>-->   
<a target="_blank" href="https://multiplierapp.atlassian.net/servicedesk/customer/portal/1"><img class = "img-zoom" src="<?php echo HTTP_ROOT ;?>img/1962073065-Help.png" id="bottom-help-img"></a> 
